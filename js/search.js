var servicesChecked = true;
var salesChecked = true;


function initSearch(senddata){
	 $("#result").kendoAutoComplete({
       dataSource: senddata,
       placeholder: "Enter County or Dealer",
       filter: "contains",
       dataTextField: "NameCity",
       //template: "#= Name #",
       //template: "#= Name # #= City #",
       height: 900,
       animation: {
		   close: {
		     effects: "fadeOut zoom:out",
		     duration: 300
		   },
		   open: {
		     effects: "fadeIn zoom:in",
		     duration: 300
		   }
		  },
       select: function(e) {

       	console.log("Select");
       	var dataItem = this.dataItem(e.item.index());	
       	var localShow = "show" + dataItem.CompanyIdentificationNumber;
        var url = dataItem.CompanyIdentificationNumber;

        if(!$('#'+url).hasClass('in')){
          $('#accordion .in').collapse('hide');
          setTimeout(function(){
	          window.location.href = "#"+localShow;
	          $(localShow).hide();
	          $('#'+url).collapse('toggle');
	          var localpoint = new google.maps.LatLng(dataItem.Latitude, dataItem.Longitude);
	          map.setZoom(12);
	          map.setCenter(localpoint);
	        }, 200, dataItem, localShow, url);
        }
    }
   });

  $('.mail .emailAddress').on('click', function (e) {
     event.stopPropagation();
    console.log("click");
  });

  $('.dealerWeb .websiteURL').on('click', function (e) {
     event.stopPropagation();
    console.log("click");
  });
}


function refreshSearch(tempData){
	console.log(tempData);
	var dataSource = new kendo.data.DataSource(tempData);
	var autoComplete = $("#result").data("kendoAutoComplete");
	autoComplete.setDataSource(dataSource);
}


function makeData(){
	$("#result").val("");
	if(salesChecked && !servicesChecked){
		var tempData = [];
		for(var i = 0; i < data.length; i++){
			if(data[i].IsSales == "true"){
				tempData.push(data[i]);
			}
			if(i === data.length-1){
				var jsArray = {
					data : tempData
				}
				console.log("Done");
  				initialize(tempData);
				refreshSearch(jsArray);
			}
		}
	}
	if(servicesChecked && !salesChecked){
		console.log("Only services");
		var tempData = [];
		for(var i = 0; i < data.length; i++){
			if(data[i].IsService == "true"){
				tempData.push(data[i]);
				console.log("index " + i);
			}
			if(i === data.length-1){
				console.log("Done");
				var jsArray = {
					data : tempData
				}
  				initialize(tempData);
				refreshSearch(jsArray);
			}
		}
	}
	if(servicesChecked && salesChecked){
		console.log("All");
		var jsArray = {
			data : data
		}
		refreshSearch(jsArray);
  		initialize(data);
	}
	if(!servicesChecked && !salesChecked){
		console.log("All");
		var jsArray = {
			data : data
		}
		refreshSearch(jsArray);
  		initialize(data);
	}
}