function initialize() {

	var myCenter=new google.maps.LatLng(53.394951,-7.784735);
	var marker;

  var mapProp = {
    center:new google.maps.LatLng(53.394951,-7.784735),
    zoom:8,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      myFunction(xhttp);
    }
  };
  xhttp.open("GET", "dealers.xml", true);
  xhttp.send();

  function myFunction(xml) {
    var i;
    var xmlDoc = xml.responseXML;
    var x = xmlDoc.getElementsByTagName("Dealer");
    for (i = 0; i <x.length; i++) {
      
      var internalURL ="#" + x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue;
      //console.log(internalURL);

      if(sales == 'true'){
        $("#pnlList ul").append('<li class="link"><button href="#' + (x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue) + '" data-toggle="collapse"><h2 class="ddealername">' + (x[i].getElementsByTagName("Name")[0].childNodes[0].nodeValue) + '</h2><p><div class="address">' + (x[i].getElementsByTagName("Street")[0].childNodes[0].nodeValue) + ', ' + (x[i].getElementsByTagName("City")[0].childNodes[0].nodeValue) + '</div></p><div id="' + (x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue) + '" class="collapse"><div class="phone">Phone<p class="phoneNumber">' + (x[i].getElementsByTagName("Telephone")[0].childNodes[0].nodeValue) + '</p></div><div class="mail">Email<a class="emailAddress" href="mailto:">' + (x[i].getElementsByTagName("Mail")[0].childNodes[0].nodeValue) + '</a></div><div class="dealerWeb"><a class="websiteURL" href="' + (x[i].getElementsByTagName("CustomWebsiteURL")[0].childNodes[0].nodeValue) + '"><img src="img/dealerWeb.png" /></a></div></div></button></li>');
        var sales = x[i].getElementsByTagName("IsSales")[0].childNodes[0].nodeValue;
        var point = new google.maps.LatLng(x[i].getElementsByTagName("Latitude")[0].childNodes[0].nodeValue, x[i].getElementsByTagName("Longitude")[0].childNodes[0].nodeValue);
        var marker=new google.maps.Marker({
          position:point,
          animation:google.maps.Animation.DROP,
          icon:'img/salesservice.png',
          url: x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue
        });
        console.log(marker.url);
        
        $(marker.url).attr("data-toggle", "collapse");
        google.maps.event.addListener(marker, 'click', function() {
          console.log(marker.url);
        });
        
        marker.setMap(map);
      
      } else {
        $("#pnlList ul").append('<li class="link"><button href="#' + (x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue) + '" data-toggle="collapse"><h2 class="ddealername">' + (x[i].getElementsByTagName("Name")[0].childNodes[0].nodeValue) + '</h2><p><div class="address">' + (x[i].getElementsByTagName("Street")[0].childNodes[0].nodeValue) + ', ' + (x[i].getElementsByTagName("City")[0].childNodes[0].nodeValue) + '</div></p><div id="' + (x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue) + '" class="collapse"><div class="phone">Phone<p class="phoneNumber">' + (x[i].getElementsByTagName("Telephone")[0].childNodes[0].nodeValue) + '</p></div></div></button></li>');

        var point = new google.maps.LatLng(x[i].getElementsByTagName("Latitude")[0].childNodes[0].nodeValue, x[i].getElementsByTagName("Longitude")[0].childNodes[0].nodeValue);
        var marker=new google.maps.Marker({
          position:point,
          animation:google.maps.Animation.DROP,
          icon:'img/service.png'
        });
        marker.setMap(map);
      }
      
    }
  }

  $('.emailAddress').on('click', function (e) {
    e.stopPropagation();
  });

  $('.websiteURL').on('click', function (e) {
    e.stopPropagation();
  });

}
google.maps.event.addDomListener(window, 'load', initialize); 


