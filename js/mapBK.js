function initialize() {

	var myCenter=new google.maps.LatLng(53.394951,-7.784735);
	var marker;

  var mapProp = {
    center:myCenter,
    zoom:8,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      myFunction(xhttp);
    }
  };
  xhttp.open("GET", "dealers.xml", true);
  xhttp.send();

  function myFunction(xml) {
    var i;
    var xmlDoc = xml.responseXML;
    var x = xmlDoc.getElementsByTagName("Dealer");
    for (i = 0; i <x.length; i++) {
      var sales = x[i].getElementsByTagName("IsSales")[0].childNodes[0].nodeValue;
      if(sales == 'true'){
        var show ="show" + x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue;
        $("#pnlList ul").append('<li class="link" id="' + show + '"><button href="#' + (x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue) + '" data-toggle="collapse"><h2 class="ddealername">' + (x[i].getElementsByTagName("Name")[0].childNodes[0].nodeValue) + '</h2><p><div class="address">' + (x[i].getElementsByTagName("Street")[0].childNodes[0].nodeValue) + ', ' + (x[i].getElementsByTagName("City")[0].childNodes[0].nodeValue) + '</div></p><div id="' + (x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue) + '" class="collapse"><div class="phone">Phone<p class="phoneNumber">' + (x[i].getElementsByTagName("Telephone")[0].childNodes[0].nodeValue) + '</p></div><div class="mail">Email<a class="emailAddress" href="mailto:">' + (x[i].getElementsByTagName("Mail")[0].childNodes[0].nodeValue) + '</a></div><div class="dealerWeb"><a class="websiteURL" href="' + (x[i].getElementsByTagName("CustomWebsiteURL")[0].childNodes[0].nodeValue) + '"><img src="img/dealerWeb.png" /></a></div></div></button></li>');
          var internalURL ="#" + x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue;
          var point = new google.maps.LatLng(x[i].getElementsByTagName("Latitude")[0].childNodes[0].nodeValue, x[i].getElementsByTagName("Longitude")[0].childNodes[0].nodeValue);
          var marker=new google.maps.Marker({
            position:point,
            animation:google.maps.Animation.DROP,
            icon:'img/salesservice.png',
            show:show,
            url:internalURL
          });

          google.maps.event.addListener(marker, 'click', function() {
            window.location.href = "#"+this.show;
            $(show).hide();
            $(this.url).collapse('toggle');
            map.setZoom(9);
            map.setCenter(this.position);
          });

        marker.setMap(map);
      } else {
        var show ="show" + x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue;
        $("#pnlList ul").append('<li class="link" id="' + show + '"><button href="#' + (x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue) + '" data-toggle="collapse"><h2 class="ddealername">' + (x[i].getElementsByTagName("Name")[0].childNodes[0].nodeValue) + '</h2><p><div class="address">' + (x[i].getElementsByTagName("Street")[0].childNodes[0].nodeValue) + ', ' + (x[i].getElementsByTagName("City")[0].childNodes[0].nodeValue) + '</div></p><div id="' + (x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue) + '" class="collapse"><div class="phone">Phone<p class="phoneNumber">' + (x[i].getElementsByTagName("Telephone")[0].childNodes[0].nodeValue) + '</p></div></div></button></li>');
            var internalURL ="#" + x[i].getElementsByTagName("CompanyIdentificationNumber")[0].childNodes[0].nodeValue;
            var point = new google.maps.LatLng(x[i].getElementsByTagName("Latitude")[0].childNodes[0].nodeValue, x[i].getElementsByTagName("Longitude")[0].childNodes[0].nodeValue);
            var marker=new google.maps.Marker({
            position:point,
            animation:google.maps.Animation.DROP,
            icon:'img/service.png',
            show:show,
            url:internalURL
            });
            
            google.maps.event.addListener(marker, 'click', function() {
              window.location.href = "#"+this.show;
              $(this.url).collapse('toggle');
              map.setZoom(9);
              map.setCenter(this.position);
            });

            marker.addListener('click', function() {
          });

        marker.setMap(map);
      }
      
    }
  }
  

  $("#result").kendoAutoComplete({
    dataTextField: "Dealer",
    height: 900,
    select: function(e) {
        var dataItem = this.dataItem(e.item.index());
        var resultShow = "#show" + kendo.stringify(dataItem.ID);
        var resultToggle = "#" + kendo.stringify(dataItem.ID);
        $(resultToggle).collapse('toggle');
        console.log(resultShow);
        window.location.href = resultShow;
        map.setCenter(google.maps.LatLng(53.998356,-7.357589));   
    },
    dataSource: {
        data: [
        { Dealer : "Eamonn Tarrant and Sons Ltd", ID : 1 },
        { Dealer : "Noel Deasy Cars Ltd", ID : 2 },
        { Dealer : "Finbarr Galvin Limited", ID : 3 },
        { Dealer : "DMG Motors Ltd", ID : 4 },
        { Dealer : "Pilsen Auto Ltd", ID : 5 },
        { Dealer : "Frank Hogan SKODA", ID : 6 },
        { Dealer : "Monaghan and Sons", ID : 7 },
        { Dealer : "Liam Lynch Motors", ID : 8 },
        { Dealer : "Lahart Garages Ltd", ID : 9 },
        { Dealer : "Ballybrittas Motors", ID : 10 },
        { Dealer : "Monaghan and Sons Castlebar", ID : 11 },
        { Dealer : "Doran Motors Ltd", ID : 12 },
        { Dealer : "Burns Car Sales", ID : 13 },
        { Dealer : "Ryan Motor Power", ID : 14 },
        { Dealer : "Martin Barrett Ltd", ID : 15 },
        { Dealer : "Sinnott Autos Ltd", ID : 16 },
        { Dealer : "Sheehy Motors", ID : 17 },
        { Dealer : "MSL Park Motors", ID : 18 },
        { Dealer : "Autosuperstore Gerry Cummiskey Ltd", ID : 19 },
        { Dealer : "Blackwater Motors", ID : 20 },
        { Dealer : "John Keane and Sons", ID : 21 },
        { Dealer : "Pierse Motors Ltd", ID : 22 },
        { Dealer : "Boland SKODA", ID : 23 },
        { Dealer : "Donohoe SKODA", ID : 24 },
        { Dealer : "Western Motors SKODA", ID : 25 },
        { Dealer : "Bradys Cavan", ID : 26 },
        { Dealer : "Michael Moore Car Sales", ID : 27 },
        { Dealer : "Spirit SKODA", ID : 28 },
        { Dealer : "Mullingar Autos", ID : 29 },
        { Dealer : "Annesley Williams", ID : 30 },
        { Dealer : "MSL Service Centre (North Dublin)", ID : 31 },
        { Dealer : "Al Hayes Motors", ID : 32 }
        ]
    }
});

var autoComplete = $("#result").data("kendoAutoComplete");

  // set width of the drop-down list
  autoComplete.list.width(400);

  $('.mail .emailAddress').on('click', function (e) {
    e.stopPropagation();
  });

  $('.dealerWeb .websiteURL').on('click', function (e) {
    e.stopPropagation();
  });

}
google.maps.event.addDomListener(window, 'load', initialize); 