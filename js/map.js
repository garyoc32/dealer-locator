var data = []; 
var map;

$(document).ready(function () {

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          convertXml2JSon(this);
      }
  };
  xhttp.open("GET", "./dealers.xml", true);
  xhttp.send();

  var x2js = new X2JS();

  function convertXml2JSon(xml) {
    var xmlDoc = xml.responseXML;
      var dealers = x2js.xml2json(xmlDoc);
      data = dealers.Dealers.Dealer;
      for(var i = 0; i < data.length; i++){
        data[i].NameCity = data[i].Name + ", " + data[i].City;
      }
      initSearch(data);
      initialize(data);
  }

  $('#MainContent_chkAfterSale').on('click', function() {
    console.log("Clicked services");
      if(this.checked) {
        servicesChecked = true;
      }else{
        servicesChecked = false;
      }
     makeData();
  });

$('#MainContent_chkSale').on('click', function() {
    console.log("Clicked sales");
      if(this.checked) {
         salesChecked = true;
      }else{
        salesChecked = false; 
      }
      makeData();
});


});

function initialize(data) {

  var myCenter=new google.maps.LatLng(53.394951,-7.784735);
  var marker;

  var mapProp = {
    center:myCenter,
    zoom:8,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  
  map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

  makeMap(data);

  function makeMap(data) {
    var i;
    var markers = [];
    $("#pnlList ul").html("");
    for (i = 0; i <data.length; i++) {

      var icon;
      if(data[i].IsSales == "true"){
        icon = 'img/salesservice.png';
      }else{
        icon = 'img/service.png';
      }

      var id = data[i].CompanyIdentificationNumber;
      var show ="show" + data[i].CompanyIdentificationNumber;

      $('#pnlList ul').append(
        '<li class="link" data-lat="'+data[i].Latitude+'" data-long="'+data[i].Longitude+'" id="' + show + '">'+
        '<div id="wrapper">'+
        '<button href="#' + id + '" id="' + show + '" data-toggle="collapse">'+
        '<h2 class="ddealername"><span id="anchor"></span>' + data[i].Name+ '</h2>'+
        '<p><div class="address">' + data[i].Street+ ', ' + data[i].City+ '</div></p></button>'+
        '<div id="' +id+ '" class="collapse"></div></div></li>');

        if(data[i].Telephone){
          $('#'+id).append('<div class="phone">Phone<a class="phoneNumber" target="_top" href="tel:'+ data[i].Full_Telephone +'">' + data[i].Telephone + '</p></div>');
        }
        if(data[i].Mail){
          $('#'+id).append('<div class="mail">Email<a class="emailAddress" target="_top" href="mailto:'+data[i].Mail+'">' + data[i].Mail + '</a></div>');
        }
        if(data[i].CustomWebsiteURL){
          $('#'+id).append('<div class="dealerWeb"><a class="websiteURL" target="_blank" href="' + data[i].CustomWebsiteURL + '"><img src="img/dealerWeb.png" /></a>');
        }
        
        var point = new google.maps.LatLng(data[i].Latitude, data[i].Longitude);
        var internalURL ="#" + data[i].CompanyIdentificationNumber;

        var marker=new google.maps.Marker({
          position:point,
          animation:google.maps.Animation.DROP,
          icon:icon,
          show: show,
          url:internalURL
        });
  
        markers.push(marker);

        $("#"+show).click(function(event) {  
          var localShow = $(this).attr("id");
          var url = localShow.substring(4, localShow.length);
          var localpoint = new google.maps.LatLng($(this).attr("data-lat"), $(this).attr("data-long"));
          if(!$('#'+url).hasClass('in')){
            console.log("Is open : " + $('#'+url).hasClass('in'))
            $('#accordion .in').collapse('hide');
            console.log("close all");
            setTimeout(function(){
              window.location.href = "#"+localShow;
              $(localShow).hide();
              console.log("open")
              $('#'+url).collapse('toggle');
              map.setZoom(12);
              map.setCenter(localpoint);
              console.log("Is open : " + $('#'+url).hasClass('in'))
            }, 200, localpoint, localShow, url);
          }else{
            event.stopPropagation();
            console.log("do nothing");
          }
          
        });

        google.maps.event.addListener(marker, 'click', function() {
          if(!$(this.url).hasClass('in')){
            $('#accordion .in').collapse('hide');
            var show = this.show;
            var url = this.url;
            var point = this.position;
            console.log(show + " "+ url + " " + point);
            setTimeout(function(){
                console.log(show + " "+ url + " " + point);
                window.location.href = "#"+show;
                $(show).hide();
                $(url).collapse('toggle');
                map.setZoom(12);
                map.setCenter(point);
            }, 200, show, url, point);
          }
          
        });

        //marker.setMap(map);
      
    }

    console.log(markers);
    var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'https://github.com/googlemaps/js-marker-clusterer/blob/gh-pages/images/m1.png?raw=true'});
  }
}
